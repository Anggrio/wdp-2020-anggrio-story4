from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'index.html')

def description(request):
    return render(request, 'description.html')

def background(request):
    return render(request, 'background.html')

def contact(request):
    return render(request, 'contact.html')

def challenge(request):
    return render(request, 'challenge.html')